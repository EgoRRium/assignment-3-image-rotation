#ifndef ROTATE_H
#define ROTATE_H
#include "image.h"
#include <stdlib.h>

void rotate(struct image* img, int angle);
#endif
