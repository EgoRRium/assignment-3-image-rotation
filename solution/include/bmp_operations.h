#ifndef BMP_OPERATIONS_H
#define BMP_OPERATIONS_H
#include "bmp_header.h"
#include "errors.h"
#include <stdio.h>
#include <stdlib.h>

enum open_status open_bmp_file_to_read(const char* fname, FILE** input_file);
enum open_status open_bmp_file_to_write(const char* fname, FILE** output_file);

enum read_status read_from_bmp(FILE** input_file, struct image *img);

enum write_status write_to_bmp(FILE** output_file, struct image *img);

enum close_status close_bmp_file(FILE** open_file);
#endif
