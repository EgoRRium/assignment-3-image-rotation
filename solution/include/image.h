#ifndef IMAGE_H
#define IMAGE_H
#include <stdint.h>
struct image {
    uint32_t width, height;
    struct pixel* pixels;
};

#pragma pack(push, 1)
struct pixel {
    uint8_t r, g, b;
};
#pragma pack(pop)
#endif
