#ifndef ERRORS_H
#define ERRORS_H
#include <stdio.h>
#include <stdlib.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum  write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR
};

void print_error(const char* err);
void find_read_error(enum read_status rs/*, FILE** open_file*/);
void find_write_error(enum write_status ws/*, FILE** open_file*/);
void find_open_error(enum open_status os/*, FILE** open_file*/);
void find_close_error(enum close_status cs);
#endif
