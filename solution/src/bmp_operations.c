#include "bmp_operations.h"
#include "errors.h"
#define size (4 - (sizeof(struct pixel) * header.biWidth) % 4)

enum open_status open_bmp_file_to_read(const char* fname, FILE** input_file) {
    *input_file = fopen(fname, "rb");
    if (input_file == NULL) {
        return OPEN_ERROR;
    }
    return OPEN_OK;
}

enum open_status open_bmp_file_to_write(const char* fname, FILE** output_file) {
    *output_file = fopen(fname, "wb");
    if (output_file == NULL) {
        return OPEN_ERROR;
    }
    return OPEN_OK;
}

enum read_status read_from_bmp(FILE** input_file, struct image *img) {
    struct bmp_header header;
    if (!fread(&header, sizeof(struct bmp_header), 1, *input_file))
        return READ_INVALID_HEADER;
    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.bfReserved != 0 || header.biPlanes != 1 ||
                header.biBitCount != 24  || header.biCompression != 0)
        return READ_INVALID_BITS;
    img->width = header.biWidth;
    img->height = header.biHeight;
    img->pixels = malloc(sizeof(struct pixel) * img->width * img->height);
    if (!img->pixels) return READ_ERROR;
    struct pixel trash_buff = {255, 255, 255};
    int garbage = img->width * 3 % 4 != 0;
    for (uint32_t i = 0; i < img->height; i++) {
        if (!fread(&img->pixels[i * img->width], sizeof(struct pixel), img->width, *input_file)) {
            return READ_INVALID_BITS;
        }
        if (garbage) {
            fread(&trash_buff, 4 - img->width * 3 % 4, 1, *input_file);
        }
    }
    return READ_OK;
}

enum close_status close_bmp_file(FILE** open_file){
    if (open_file != NULL) {
        fclose(*open_file);
        *open_file = NULL;
        return CLOSE_OK;
    }
    return CLOSE_ERROR;
}

enum write_status write_to_bmp(FILE** output_file, struct image *img){
    struct bmp_header header = fill(img);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, *output_file)) {
        return WRITE_ERROR;
    }
    struct pixel trash_buff = {255, 255, 255};
    int garbage = img->width * 3 % 4 != 0;
    for (uint32_t i = 0; i < img->height; i++) {
        if (!fwrite(&img->pixels[i * img->width], sizeof(struct pixel), img->width, *output_file)) {
            return WRITE_ERROR;
        }
        if (garbage) {
            if (!fwrite(&trash_buff, size, 1, *output_file)){
                return WRITE_ERROR;
            }
        }
    }
    if (!fwrite(&trash_buff, size, 1, *output_file)) return WRITE_ERROR;

    return WRITE_OK;
}
