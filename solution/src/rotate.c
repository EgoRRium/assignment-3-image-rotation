#include "rotate.h"
void rotate(struct image* img, int angle) {
    if (angle < 0)
        angle = 360 + angle;
    while (angle > 0) {
        uint32_t width = img->width;
        uint32_t height = img->height;
        struct image temporary_img = (struct image) {height, width, malloc((sizeof(struct pixel) * height * width))};
        if (temporary_img.pixels != NULL) {
            for (uint32_t i = 0; i < height; i++) {
                for (uint32_t j = 0; j < width; j++) {
                    temporary_img.pixels[(width - 1 - j) * height + i] = img->pixels[i * img->width + j];
                }
            }
            if (img->pixels != NULL) {
                free(img->pixels);
                img->pixels = NULL;
            }
            img->height = temporary_img.height;
            img->width = temporary_img.width;
            img->pixels = temporary_img.pixels;
        }
        angle -= 90;
    }
}
