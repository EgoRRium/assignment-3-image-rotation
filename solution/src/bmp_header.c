#include "bmp_header.h"
#define PPermeter 0x00000B12
#define plane 0x0001
#define type 0x4D42
#define count 0x0018
#define size 0x0028
#define imgSize (image->width * sizeof(struct pixel) + (4 - image->width * sizeof(struct pixel) % 4)) * image->height

struct bmp_header fill(struct image* image) {
    return (struct bmp_header) {
        .bfType = type,
        .bfileSize = imgSize + sizeof(struct bmp_header),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = size,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = plane,
        .biBitCount = count,
        .biCompression = 0,
        .biSizeImage = imgSize,
        .biXPelsPerMeter = PPermeter,
        .biYPelsPerMeter = PPermeter,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}
