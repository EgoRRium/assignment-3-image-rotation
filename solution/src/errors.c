#include "errors.h"

const char* string_open_error[] = {
        [OPEN_ERROR] = "Open error"
};

const char* string_read_error[] = {
        [READ_INVALID_SIGNATURE] = "Invalid bfType",
        [READ_INVALID_HEADER] = "Invalid bmp header",
        [READ_INVALID_BITS] = "Invalid bits",
        [READ_ERROR] = "Unexpected close error"
};

const char* string_write_error[] = {
        [WRITE_ERROR] = "Write error"
};

const char* string_close_error[] = {
        [CLOSE_ERROR] = "Close error"
};

void print_error(const char* err) {
    printf("%s", err);
    exit(1);
}

void find_open_error(enum open_status os/*, FILE** open_file*/) {
    if (os != OPEN_OK) {
        print_error(string_open_error[os]);
    }
}

void find_read_error(enum read_status rs/*, FILE** open_file*/) {
    if (rs != READ_OK) {
        print_error(string_read_error[rs]);
    }
}

void find_write_error(enum write_status ws/*, FILE** open_file*/) {
    if (ws != WRITE_OK) {
        print_error(string_write_error[ws]);
    }
}

void find_close_error(enum close_status cs) {
    if (cs != CLOSE_OK) {
        print_error(string_close_error[cs]);
    }
}
