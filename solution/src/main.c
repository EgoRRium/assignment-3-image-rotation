#include "bmp_operations.h"
#include "rotate.h"


int main( int argc, char** argv ) {
    if (argc !=4) {
        print_error("Input must be: ./image-transformer <source-image> <transformed-image> <angle>");
    }

    FILE *input_file = NULL;
    enum open_status openStatus;
    openStatus = open_bmp_file_to_read(argv[1], &input_file);
    find_open_error(openStatus/*, &input_file*/);

    struct image img = {0};
    enum read_status readStatus;
    readStatus = read_from_bmp(&input_file, &img);
    find_read_error(readStatus/*, &input_file*/);

    enum close_status closeStatus;
    closeStatus = close_bmp_file(&input_file);
    find_close_error(closeStatus);

    int angle = atoi(argv[3]);
    if (angle % 90 != 0 && argv[3][0] != '0') {
        print_error("Angle must be: 0, 90, -90, 180, -180, 270, -270");
    }

    if (angle != 0) rotate(&img,angle);

    FILE *output_file = NULL;
    enum open_status openStatusW;
    openStatusW = open_bmp_file_to_write(argv[2], &output_file);
    find_open_error(openStatusW/*, &output_file*/);

    enum write_status writeStatus;
    writeStatus = write_to_bmp(&output_file, &img);
    find_write_error(writeStatus/*, &output_file*/);

    enum close_status closeStatusW;
    closeStatusW = close_bmp_file(&output_file);
    find_close_error(closeStatusW);

    free(img.pixels);
    return 0;
}
